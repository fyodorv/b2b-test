<?php
// Как правило, в $_GET['user_ids'] должна приходить строка
// с номерами пользователей через запятую, например: 1,2,17,48
$data = load_users_data($_GET['user_ids']);
foreach ($data as $user_id => $name) {
    echo "<a href=\"/show_user.php?id=$user_id\">$name</a>";
}


/**
 * @param $user_ids
 * @return array
 */
function load_users_data($user_ids){

    $result = [];
    $arUserIds = filterParams($user_ids);
    if (!$arUserIds) return $result;
    $pdo = getPDOConnection();

    //получение строки вида ?,?,?
    $place_holders = implode(',', array_fill(0, count($arUserIds), '?'));

    $sql = "SELECT * FROM users WHERE id in ($place_holders)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute($arUserIds);

    while ($user = $stmt->fetchObject()) {
        $result[$user->id] = $user->name;
    }

    $pdo = null;
    return $result;
}



/**
 * @param $user_ids
 * @return array
 */
function filterParams($user_ids)
{
    $arUserIds = explode(',', $user_ids);
    $arUserIds = array_map(function($id){
        return intval($id);
    },$arUserIds);
    $arUserIds = array_filter($arUserIds);
    return array_values($arUserIds);
}


/**
 * @return PDO
 */
function getPDOConnection()
{

    $host = 'localhost';
    $db   = 'database';
    $user = 'root';
    $pass = '123123';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    return new PDO($dsn, $user, $pass);
}

