<?php

$url = 'https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3';
$mustBe = 'https://www.somehost.com/?param4=1&param3=2&param1=4&url=%2Ftest%2Findex.html';

$res = updateUrl($url,3);

var_dump($res);
echo $res===$mustBe?'Success':'Fail';


function updateUrl( $url, $removeValue = null)
{

    $params = parse_url($url, PHP_URL_QUERY);
    $path = parse_url($url, PHP_URL_PATH);
    $scheme = parse_url($url, PHP_URL_SCHEME);
    $host = parse_url($url, PHP_URL_HOST);

    parse_str($params,$arParams);

    //1. удалит параметры со значением “3”;
    $arParams = array_filter($arParams,function($param) use ($removeValue) {
        return $param!=$removeValue;
    });

    //2. отсортирует параметры по значению;
    asort($arParams);


    //3. добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);
    $arParams['url'] = $path;

    return "$scheme://$host/?".http_build_query($arParams);

}