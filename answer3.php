<?php
/*
Напишите код в парадигме ООП, соответствующий следующей структуре.
Сущности: Пользователь, Статья.
Связи: Один пользователь может написать несколько статей. У каждой статьи может быть только один пользователь-автор.
Функциональность:
• возможность для пользователя создать новую статью;
• возможность получить автора статьи;
• возможность получить все статьи конкретного пользователя;
• возможность сменить автора статьи.
Если вы применили какие-либо паттерны при написании, укажите какие и с какой целью.
Код, реализующий конкретную функциональность, не требуется, только общая структура классов и методов.
Код должен быть прокомментирован в стиле PHPDoc.*/


/**
 * Interface Author
 */
interface Author {
    /**
     * @return mixed
     */
    function createArticle($title);

    /**
     * @return mixed
     */
    function getArticles();
}

/**
 * Interface Article
 */
interface Article {
    /**
     * @return Author
     */
    function getAuthor();

    /**
     * @param Author $author
     * @return mixed
     */
    function setAuthor(Author $author);

    /**
     * @param $title
     * @return mixed
     */
    function setTitle($title);

    /**
     * @return mixed
     */
    function getTitle();

}


/**
 * Interface Repository
 */
interface Repository {
    /**
     * @param Article $article
     * @return mixed
     */
    function insert(Article $article);

    /**
     * @param Article $article
     * @return mixed
     */
    function remove(Article $article);

    /**
     * @param Author $user
     * @return mixed
     */
    function getList(Author $user);

    /**
     * @param $name
     * @return mixed
     */
    function getByName($name);

    /**
     * @return Article;
     */
    function createArticle();
}




/**
 * Class User
 */
class User implements Author {


    /**
     * @var
     */
    protected $name;


    /**
     * @var Repository
     */
    protected $repository;

    /**
     * User constructor.
     * @param Repository $repository
     */
    function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $title
     */
    public function createArticle($title)
    {
        $newArticle = $this->repository->createArticle();
        $newArticle->setTitle($title);
        $newArticle->setAuthor($this);
        $this->repository->insert($newArticle);
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->repository->getList($this);
    }


}


/**
 * Class ArticlesRepository
 */
class ScienceArticlesRepository implements Repository {

    /**
     * @var Article[]
     */
    protected $collection;

    /**
     * @param Article $article
     */
    public function insert(Article $article){

    }

    /**
     * @param Article $article
     */
    public function remove(Article $article){

    }

    /**
     * @return Article[]
     */
    public function getList(Author $author)
    {
        //выборка статей по автору
    }


    /**
     * @param $name
     * @return Article
     */
    public function getByName($name)
    {
        //todo ищем статью по имени. считаем что имя уникальное
        return $article;
    }

    /**
     * @return Article
     */
    public function createArticle()
    {
        // TODO: Implement createArticle() method.
    }

}


/**
 * Class SomeArticle
 */
class PhysicsArticle implements Article {

    /**
     * Author @var
     */
    protected $author;

    /**
     * @var
     */
    protected $title;


    /**
     * @param $title
     */
    public function setTitle( $title )
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param Author $author
     */
    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }

}



$rep = new ScienceArticlesRepository();

$ivanov = new User($rep);
$petrov = new User($rep);

//возможность для пользователя создать новую статью
$ivanov->createArticle('first article');
$ivanov->createArticle('second article');
$petrov->createArticle('third article');

$firstArticle = $rep->getByName('first article');

//возможность получить автора статьи;
$firstArticleAuthor = $firstArticle->getAuthor();

//возможность сменить автора статьи
$firstArticle->setAuthor($petrov);

//возможность получить все статьи конкретного пользователя
var_dump( $petrov->getArticles() );
